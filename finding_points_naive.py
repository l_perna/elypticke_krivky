import random

#Porovnava levou stranu a pravou stranu rovnice elipticke krivky
#Funkcni pro tvar y^2 = x^3 + ax + b
def isPointOnCurveNaive(y, x, a, b, q):
    modulo_1 = (pow(x, 3) + a*x + b) % q
    modulo_2 = pow(y, 2) % q
    if(modulo_1 == modulo_2):
        return True
    else:
        return False

#Prochazi mozne body dle zadane mohutnosti q
#Oba cykly prochazi cisla od 0 po q-1
#Ty se potom vkladaji do funkce isPointOnCurveNaive, kde se porovnava
#Leva strana a prava strana modulo q
#Pokud se rovnaji vlozi se do listu bodu
def findPointsNaive(a, b, q):
    points = []
    for i in range(0,q):
        for j in range(0,q):
            result = isPointOnCurveNaive(j, i, a, b, q)
            if(result):
                points.append([i,j])
    # for l in points:
    #    print(l)
    return points

#Vybere nahodny bod z listu vytvoreneho funkce fondPointsNaive    
def getPointFromCurve(a, b, q):
    points = findPointsNaive(a, b, q)
    point = random.choice(points)
    print("Bod je ", point)
    return
