import random
import math

def isPointOnCurveNaive(y, x, a, b, q):
    modulo_1 = (pow(x, 3) + a*x + b) % q
    modulo_2 = pow(y, 2) % q
    if(modulo_1 == modulo_2):
        return True
    else:
        return False
    
def findPointsNaive(a, b, q):
    points = []
    for i in range(0,q):
        for j in range(0,q):
            result = isPointOnCurveNaive(j, i, a, b, q)
            if(result):
                points.append([i,j])
    # for l in points:
    #    print(l)
    return points

def isPrime(num):
    if num > 1:
       for i in range(2,num):
           if (num % i) == 0:
               return False
       else:
           return True
    else:
      return False

def checkCurve(a, b, p):
    if(not isPrime(p)):
       print("Not a prime number.")
       return
    if(4*pow(a,3) + 27 * pow(b,2) % p == 0):
        return False
    else:
        return True
    

def getPointFromCurve(a, b, q):
    points = findPointsNaive(a, b, q)
    point = random.choice(points)
    print("Bod je ", point)
    return

def egcd(a, b):
    t = u = 1
    s = v = 0
    while b:
        a, (q, b) = b, divmod(a, b)
        u, s = s, u-q * s
        v, t = t, v-q*t
    return a, u, v


def point_double_old(X, Y, P, a):
    deli = egcd( 2*Y, P)
    #print(deli)
    s = ((3*pow(X, 2) + a)* deli[1]) % P
    #print(s)
    x = (pow(s, 2) - 2*X) % P
    y = (s*(X - x) - Y) % P
    return [x, y]

def point_add_old(px, py, qx, qy, P):
    if(px ==0 and py ==0):
        return [qx, qy]
    if(qx == 0 and qy == 0):
        return [px, py]
    deli = egcd(qx-px, P)
    if(deli[0] != 1):
        return [deli[0], -1]
    s = ((qy - py) * deli[1]) % P
    x = (pow(s, 2) - px - qx) % P
    y = (s*(px - x) - py) % P
    return [x, y]

def ECDHFirst(P, d, X, Y, a, b):
    if(not isPrime(P)):
        print("P is not a prime number")
        return
    if(not checkCurve(a, b, P)):
        print("Not valit elliptic curve")
        return
    Bod = multi(X, Y, d, P, a)
    print("Vysledny bod je [" +str(Bod[0])+","+ str(Bod[1]) + "], Ktery poslete druhemu ucastnikovi, pamatujte si cislo d: " + str(d)+".")
    return

def ECDHsecond(X, Y, d, P, a, b):
    if(not isPrime(P)):
        print("P is not a prime number")
        return
    if(not checkCurve(a, b, P)):
        print("Not valit elliptic curve")
        return
    Bod = multi(X, Y, d, P, a)
    print("Vysledny bod je [" +str(Bod[0])+","+ str(Bod[1]) + "].")
    return

def multi_old(px, py, n,P, a):
    r = [0,0]
    for b in bin(n)[2:]:
        #print(b)
        r = point_double_old(r[0], r[1],P, a)
        if b =='1':
            r = point_add_old(r[0], r[1],px, py, P)
            if(r[1] == -1):
                return r
        #print(r)
    return r

def inversion(P, x, y):
    y1 = (P - y)
    return [x, y1]

def point_addition(px, py, qx, qy, P, pz = 1, qz = 1):
    if(px ==0 and py ==0):
        return [qx, qy, qz]
    if(qx == 0 and qy == 0):
        return [px, py, pz]
    T0 = (py * qz )% P
    T1 = (qy * pz)% P
    T = (T0 - T1)% P
    U0 = (px*qz)% P
    U1 = (qx*pz)% P
    U = (U0 - U1)% P
    U2 = (pow(U, 2))% P
    V = (pz * qz) % P
    W = (pow(T, 2)*V - U2*(U0+U1))% P
    U3 = (U * U2)% P
    X = (U*W)% P
    Y = (T*(U0*U2-W)-T0*U3)% P
    Z = (U3 * V)% P
    return [X,Y,Z]

def point_doubling(X, Y, P, a, Z = 1):
    if(X == 0 and Y == 0):
        return [0, 0, 1]
    T = (3*pow(X,2) + a*pow(Z, 2) )% P
    U = (2*Y*Z)% P
    V = (2*U*X*Y)% P
    W = (pow(T, 2)-2*V)% P
    X2 = (U *W)% P
    Y2 = (T*(V-W)-2 * pow(U*Y, 2))% P
    Z2 = (pow(U, 3))% P
    return [X2, Y2, Z2]

def point_multiplication(px, py, n, P, a):
    r = [0,0,1]
    for b in bin(n)[2:]:
        r = point_doubling(r[0], r[1],P, a, r[2])
        if b =='1':
            r = point_addition(r[0], r[1],px, py, P, 1, r[2])
    point = [(r[0] * egcd(r[2], P)[1] )%P, (r[1] * egcd(r[2], P)[1]) %P]
    return point
    
def lestra_factorization(N):
    if(isPrime(N)):
        return "Prime"
    while True:
        A = random.randint(1, 512) % N
        a = random.randint(1, 512) % N
        b = random.randint(1, 512) % N
        B = (pow(b,2) - pow(a,3) - A*a) % N
        if(B % N != 0):
            break
        print(N)
    P = [a, b]
    for i in range(1, N):
        p = multi_old(P[0], P[1], i, N, a)
        if(p[1] == -1):
            other = N // p[0]
            return [p[0],other]
    return [N]



def smallbig(a, b, q):
    small = math.sqrt(math.sqrt(q))
    if(isinstance(small,float)):
        #print("Float")
        small = math.ceil(small)
    elif(isinstance(small, int)):
        #print("Int")
        small += 1
    #print(small)
    points = findPointsNaive(a, b, small)
    #interval_bottom = q+1-math.sqrt(q)
    #interval_upper = q+1+math.sqrt(q)
    
    
    randpoint = random.choice(points)
    points_new = []
    for i in range(1, small+1):
        new_point = point_multiplication(randpoint[0],randpoint[1],i,q,a)
        points_new.append(new_point)
        points_new.append(inversion(q, new_point[0],new_point[1]))
    Q = point_multiplication(randpoint[0], randpoint[1], 2*small+1, q, a)
    R = point_multiplication(randpoint[0], randpoint[1], q+1, q, a)
    t = math.ceil((2*math.sqrt(q))/(2*small+1))
    Qneg = inversion(q, Q[0], Q[1])
    j = 0
    for i in range(0, t+1):
        Qmulti = point_multiplication(Q[0], Q[1], i, q, a)
        RQpoint = point_add_old(R[0], R[1], Qmulti[0], Qmulti[1],q)
        if(RQpoint in points_new):
            j = i
            break
        
    for i in range(0, t+1):
        Qmulti = point_multiplication(Qneg[0], Qneg[1], i, q, a)
        RQpoint = point_add_old(R[0], R[1], Qmulti[0], Qmulti[1],q)
        if(RQpoint in points_new):
            if (j == 0):
                j = i
            break
    k = 0


    
    for i in range(1, small+1):
        new_point = point_multiplication(randpoint[0],randpoint[1],i,q,a)
        if(RQpoint == new_point):
            k = i
            break
        if(RQpoint == inversion(q, new_point[0], new_point[1])):
            k = i
            break
    m = q+1+(2*small+1)*j*i
    print(m)
    return  point_multiplication(randpoint[0],randpoint[1],m,q,a)

def Legendre(a, X):
    legendre = (pow(a, (X-1)/2) %X)
    if(legendre == (-1)%X):
        return -1
    elif(legendre == (1)%X):
        return 1
    else:
        return 0
    return 

def cipolla(n, X):
    leg = Legendre(n, X)
    if (leg != 1):
        return -1
    while(True):
        a = random.randint(1, X)
        notsquare = Legendre(a*a-n,X)
        print(notsquare)
        if(notsquare == -1.0):
            w = (a*a - n) % X
            print(w)
            temp = pow(a+ math.sqrt(w),(X+1)/2)
            break
    return temp
