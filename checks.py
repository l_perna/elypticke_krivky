#Kontroluje, jestli je vlozene cislo prrvocislem
def isPrime(p):
    if p > 1:
        for i in range (2, p//2):
            if(p % i == 0):
                return False
            else:
                return True
    else:
        return False

#Kontroluje, jestli zadane parametry a prvocislo tvori elyptickou krivku
#Podle vzorce 4*a^3 + 27b^2 != 0 mod p
#Plati pro Weierstrassuv tvar

def checkCurve(a, b, p):
    if(not isPrime(p)):
       print("Not a prime number.")
       return
    if(4*pow(a,3) + 27 * pow(b,2) % p == 0):
        return False
    else:
        return True
