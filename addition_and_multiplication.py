#Rozsireny Eukliduv augoritmus, pro vypocet modularni multiplikativni inverzi,
#ktera je potrebna pro scitani a znasobeni bodu
def egcd(a, b):
    t = u = 1
    s = v = 0
    while b:
        a, (q, b) = b, divmod(a, b)
        u, s = s, u-q * s
        v, t = t, v-q*t
    return a, u, v

#Znasobeni bodu, zadany souradnice bodu X a Y, prvocislo P a parametr a krivky
#Protoze je ve vypoctu smernice deleni, musime vypocet upravit a misto deleni
#vynasobime citatel modularni multiplikativni inverzi jmenovatele
#rovnice jsou upravovany modulem pro agoritmus ECDH
def point_double(X, Y, P, a):
    mmi = egcd( 2*Y, P)
    s = ((3*pow(X, 2) + a)* mmi[1]) % P
    x = (pow(s, 2) - 2*X) % P
    y = (s*(X - x) - Y) % P
    return [x, y]

#Podobne jako u znasobeni bodu, akorat jsou rovnice upravene, opet je potreba
#modularni multiplikativni inverze
def point_add(px, py, qx, qy, P):
    if(px ==0 and py ==0):
        return [qx, qy]
    if(qx == 0 and qy == 0):
        return [px, py]
    mmi = egcd(qx-px, P)
    s = ((qy - py) * mmi[1]) % P
    x = (pow(s, 2) - px - qx) % P
    y = (s*(px - x) - py) % P
    return [x, y]

#Nekolikanasobne scitani a znasobeni bodu, 
def multi(px, py, n,P, a):
    r = [0,0]
    for b in bin(n)[2:]:
        r = point_double(r[0], r[1],P, a)
        if b =='1':
            r = point_add(r[0], r[1],px, py, P)
    return r
