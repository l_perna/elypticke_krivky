#Prvni funkce, ucastnik vlozi parametry krivky, zvoleny bod na krivce, 
#zvolene prvocislo a soukromy klic d, algoritmus vytvori novy bod pomoci
#nasobeni a scitani bodu, ktery se nakonec vypise na konzoli
def ECDHFirst(P, d, X, Y, a, b):
    if(not isPrime(P)):
        print("P is not a prime number")
        return
    if(not checkCurve(a, b, P)):
        print("Not valit elliptic curve")
        return
    Bod = multi(X, Y, d, P, a)
    print("Vysledny bod je [" +str(Bod[0])+","+ str(Bod[1]) + "], Ktery poslete druhemu ucastnikovi, pamatujte si cislo d: " + str(d)+".")
    return

#Vlozi se bod prijaty od druheho ucastnika, ten se vlozi spolu s mym klicem d a
#parametry krivky, vypocita se novy bod, ktery by mnel byt pro oba ucastniky
#stejny
def ECDHsecond(X, Y, d, P, a, b):
    if(not isPrime(P)):
        print("P is not a prime number")
        return
    if(not checkCurve(a, b, P)):
        print("Not valit elliptic curve")
        return
    Bod = multi(X, Y, d, P, a)
    print("Vysledny bod je [" +str(Bod[0])+","+ str(Bod[1]) + "].")
    return

#Funkce popsany v souboru addition and multiplication
def multi(px, py, n ,P, a):
    r = [0,0]
    for b in bin(n)[2:]:
       # print(b)
        r = point_double(r[0], r[1],P, a)
        if b =='1':
            #print("add")
            r = point_add(px, py, r[0], r[1],P)
        #print(r)
    return r

def egcd(a, b):
    t = u = 1
    s = v = 0
    while b:
        a, (q, b) = b, divmod(a, b)
        u, s = s, u-q * s
        v, t = t, v-q*t
    return a, u, v


def point_double(X, Y, P, a):
    deli = egcd( 2*Y, P)
    #print(deli)
    s = ((3*pow(X, 2) + a)* deli[1]) % P
    #print(s)
    x = (pow(s, 2) - 2*X) % P
    y = (s*(X - x) - Y) % P
    return [x, y]

def point_add(px, py, qx, qy, P):
    if(px ==0 and py ==0):
        return [qx, qy]
    if(qx == 0 and qy == 0):
        return [px, py]
    mmi = egcd(qx-px, P)
    s = ((qy - py) * mmi[1]) % P
    x = (pow(s, 2) - px - qx) % P
    y = (s*(px - x) - py) % P
    return [x, y]

#funkce popsany v souboru checks
def isPrime(p):
    if p > 1:
        for i in range (2, p//2):
            if(p % i == 0):
                return False
            else:
                return True
    else:
        return False

def checkCurve(a, b, p):
    if(not isPrime(p)):
       print("Not a prime number.")
       return
    if(4*pow(a,3) + 27 * pow(b,2) % p == 0):
        return False
    else:
        return True
